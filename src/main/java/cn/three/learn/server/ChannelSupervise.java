package cn.three.learn.server;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

/**
 * @Description: 作用描述
 * @Author: hmm
 * @CreateDate: 2019/7/26$ 22:10$
 * @UpdateUser: hmm
 * @UpdateDate: 2019/7/26$ 22:10$
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
public class ChannelSupervise {
    private static ChannelGroup GlobalGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    //private static ConcurrentMap<String, ChannelId> ChannelMap = new ConcurrentHashMap();

    public static void addChannel(Channel channel) {
        GlobalGroup.add(channel);
        //ChannelMap.put(channel.id().asShortText(), channel.id());
    }

    public static void removeChannel(Channel channel) {
        GlobalGroup.remove(channel);
        //ChannelMap.remove(channel.id().asShortText());
    }

    /*public static Channel findChannel(String id) {
        return GlobalGroup.find(ChannelMap.get(id));
    }*/

    public static int count() {
        return GlobalGroup.size();
    }

    public static void sendToAll(TextWebSocketFrame tws) {
        GlobalGroup.writeAndFlush(new TextWebSocketFrame(tws.text().toUpperCase()));
    }
}
