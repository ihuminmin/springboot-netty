package cn.three.learn;

import cn.three.learn.server.NettyServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Description: 服务启动类
 * @Author: hmm
 * @CreateDate: 2019/7/26$ 20:46$
 * @UpdateUser: hmm
 * @UpdateDate: 2019/7/26$ 20:46$
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@SpringBootApplication
public class NettyApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(NettyApplication.class);
        NettyServer nettyServer = context.getBean(NettyServer.class);
        nettyServer.run();
    }
}
